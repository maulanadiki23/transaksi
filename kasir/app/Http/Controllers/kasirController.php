<?php

namespace App\Http\Controllers;
use Illuminate\Support\Str;

use App\Models\master_barang;
use App\Models\transaksi_pembelian;
use App\Models\transaksi_pembelian_barang;
use Illuminate\Support\Facades\DB;
use Validator;
use File;
use Image;
use Auth;

use Illuminate\Http\Request;

class kasirController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        return view('layout.index');
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $barang=DB::table('master_barang')->paginate(10);
        return view('kasir.create_trans',['barang'=>$barang] );
    }

    public function tabel()
    {
        $barang = DB::table('transaksi_pembelian')->rightJoin('transaksi_pembelian_barang','transaksi_pembelian.created_at','=','transaksi_pembelian_barang.created_at')->rightJoin('master_barang','transaksi_pembelian_barang.master_barang_id','=','master_barang.idbrg')->get();
        return view('table.table',['barang'=>$barang]);
    }


    public function simpan(request $request )
    {
        $data=$request->all();
        $a = $request->beli;
        $transbar = DB::table('transaksi_pembelian')->count();
        $transbarr = DB::table('transaksi_pembelian_barang')->count();

        if(count(array($data['kobar']>0 ))){
            foreach ($data['kobar'] as $isi => $value) {
                // pembelian barang
                $data2=array(
                    'transaksi_pembelian_id'=>date('dmy')+ $data['id'][$isi]+ $transbarr++,
                    'master_barang_id'=>$data['kobar'][$isi],
                    'jumlah'=>$data['qty'][$isi],
                    'harga_satuan'=>$data['satuan'][$isi]
                    
                );
                transaksi_pembelian_barang::create($data2);

                $total = $data['qty'][$isi] * $data['satuan'][$isi];
                $grandtotal=0;

                $grandtotal += $total;
                $data3=array(
                    
                    'id'=>date('dmy')+$transbar++,
                    'total_harga'=> $grandtotal
                );
                transaksi_pembelian::create($data3);
                
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
