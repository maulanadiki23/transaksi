<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class master_barang extends Model
{
    use HasFactory;
    protected $table ='master_barang';
    protected $primaryKey = 'id';
    public $incrementing = false;

    protected $fillable = [
        'id','nama_barang','harga_satuan','created_at','update_at'
    ];
}
