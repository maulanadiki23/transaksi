<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class transaksi_pembelian_barang extends Model
{
    use HasFactory;
    protected $table ='transaksi_pembelian_barang';
    protected $primaryKey = 'id';
    public $incrementing = false;

    protected $fillable = [
        'id','transaksi_pembelian_id','master_barang_id','jumlah','harga_satuan','created_at','upload_at'
    ];
}
