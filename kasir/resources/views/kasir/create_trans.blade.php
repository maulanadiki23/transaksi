@extends('layout.main')
@section('title','Create Transaction')
@section('container')
<section id="pembelian">
    <div class="container-fluid mt-4">
        <div class="row text-center" style="min-width: 1100px; width:100%;">
            <div class="col-md-12"><h2 id="tag">FORM PEMBELIAN BARANG</h2></div>
        
            <div class="col-md-12">
                <form action="{{route ('simpan')}}" method="post"> 
                    @csrf
                    <table class="table table-hover mt-2" id="kotak" style=" height:200px; overflow:scroll">
                            <thead class="">
                                <tr class="table-primary">
                                <th class="col-md-1">No.</th>
                                <th class="col-md-2">ID</th>
                                <th class="col-md-4">Nama Barang</th>
                                <th class="col-md-3">Harga Satuan</th>
                                <th class="col-md-2">qty</th>
                                </tr>
                            </thead>
                            <tbody id="tbody">

                            </tbody>
                        </table>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-1 "><button type="button" class="btn btn-outline-success" data-bs-toggle="modal" data-bs-target="#item">ADD</button></div>
                        <div class="col-md-1 "><button type="button" class="btn btn-outline-danger ms-5" id="kurang"> DELETE</button></div>
                        <div class="col-md-10  d-flex justify-content-end"><button type="submit" class="btn btn-outline-primary" id="save">SAVE</button></div>
                    </div>    
                </div>     
            </form>
            </div>

        </div>
    </div>


<div class="modal fade" id="item" data-bs-keyboard="false" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">List Items</h5>
            </div>
            <div class="modal-body">
                <!-- cardnya disini ya -->


                <div class="input-group col-mb-12">
                    <input type="text" class="form-control" placeholder="Masukan nama Barang" name="inputcari" id="inputcari" aria-label="Nama Barang" aria-describedby="button-addon2">
                </div>

                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Nama Barang</th>
                            <th scope="col">Harga Satuan</th>
                            
                        </tr>
                    </thead>
                    <tbody class="search-result" id="hasil">
                        @foreach($barang as $brg)
                        <tr>
                            <td scope="row">{{$loop->iteration}}</td>
                            <td><a href="#" data-bs-dismiss="modal" id="itemss" style="text-decoration:none; color:black; font: size 18px;" data-nabar="{{$brg->nama_barang}}" data-id="{{$brg->id}}" data-kobar="{{$brg->id}}" data-satuan="{{$brg->harga_satuan}}" >{{$brg->nama_barang}}</a></td>
                            <td>{{$brg->harga_satuan}}</td>       
                        </tr>

                        @endforeach
                        {{$barang->links() }}
                    </tbody>

                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>    
</div>


<script type="text/javascript">
$(document).ready(function() {
                    $(document).on('click', '#itemss', function() {
                        const nabar = $(this).data('nabar');
                        const kobar= $(this).data('kobar');
                        const satuan= $(this).data('satuan');
                        const id= $(this).data('id');
                        let kolom = $('#tbody tr').length;
                        $no = kolom +1;
                        var baris1 = '<tr align="center">'
                        +'<td>'+ $no +'</td>'
                        +'<td><input type="hidden" name="id[]" value="'+ id +'"> <input type="hidden" name="kobar[]" value="'+ kobar +'" > '+ kobar +'</td>'
                        +'<td>'+ nabar+'</td>'
                        +'<td> <input type="hidden" name="satuan[]" value="'+ satuan +'" >'+ satuan +'</td>'
                        +'<td><input type="number" name="qty[]"></td>'
                        +'</tr>'
                        $("#tbody").append(baris1);
                    });
                });
                $(document).ready(function() {
                    $(document).on('click', '#kurang', function() {

                    
                        $('#tbody tr:last').remove();
                    });
                });
</script>
@endsection