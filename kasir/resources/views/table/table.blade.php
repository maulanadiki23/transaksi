@extends('layout.main')
@section('title','Table Transaksi')
@section('container')
<div class="container-fluid">

                    
                    

                    <!-- DataTales Example -->
                    <div class="card shadow mb-4 mt-5">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">DataTables Transaksi</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>kode barang</th>
                                            <th>Nama Barang</th>
                                            <th>Harga Satuan</th>
                                            <th>Qty</th>
                                            <th>subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($barang as $brg)
                                    <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$brg->id}}</td>
                                    <td>{{$brg->nama_barang}}</td>
                                    <td>{{$brg->harga_satuan}}</td>
                                    <td>{{$brg->jumlah}}</td>
                                    @php
                                        $total = $brg->jumlah * $brg->harga_satuan;
                                    @endphp
                                    <td>{{$total}}</td>
</tr>
                                    @endforeach
                                    
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

</div>
@endsection